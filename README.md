# project-carwash

el siguiente proyecto se estara trabajando lo referente al proyecto de la app movil de autolavados.
aqui se encontraran todos los archivos referentes para ejecutar el proyecto.

para mas informacion sobre la documentacion de **Flutter** visitar [el sitio oficial](https://flutter-es.io/)


## Requerimientos.
* Dart version (actualmente) : 2.8.0.
* Flutter Version (actualmente): 1.15.2.
* Firebase.

### Librerias utilizadas (pubspec.yml)
* http: ^0.12.0+2
* provider: ^3.2.0
* font_awesome_flutter: ^8.5.0
* badges: ^1.1.0
* firebase_auth: ^0.15.3
* modal_progress_hud: ^0.1.3
* cloud_firestore: ^0.13.0+1
* google_maps_flutter: ^0.5.21+15
* google_sign_in: ^4.1.1
* get_it: ^3.1.0
* intl: ^0.16.1
* flutter_facebook_login: ^3.0.0

## Estructura de carpetas
la estructura basica de un proyecto utilizando la libreria **Provider** es la siguiente
* Core: donde iran todos los archivos de la logica.
* UI: donde iran todos los archivos de las vistas.
* **locator.dart**: archivo fundamental desde donde se realizaran las injecciones de dependecia.
* routes.dart: Donde se estaran guardando todas las rutas que se utilizaran en la aplicacion.

### 1. Core.
dentro de esta carpeta encontramos 3 directorios mas dividos en 
* **controllers** donde se encuentran los archivos para realizar las acciones empleando las respectivas apis, ademas de eso
tambien encontraremos un archivo especial  **BaseModel** el cual es una "plantilla" que manejara el estado global de la aplicacion, de esta
heredan todos los archivos que se encuentran en esta carpeta:
    1. BaseModel.dart
    2. CarModel.dart
    3. ReviewsModel.dart
    4. ServiciosModel.dart
    5. UserModel.dart
* **Models** el cual contiene los modelos para cada uno de los objetos con los que trabajamos en la app:
    1. Carwas.dart
    2. Reviews.dart
    3. Servicios.dart
    4. User.dart
    5. Turn.dart
* **Services** aqui, se tienen todos los archivos que realizan peticiones directamente con la api de Firestore, en ella podemos encontrar:
    1. Api.dart. Donde se encuentran todos los metodos que emplean los usuarios.
    2. AuthServices.dart. Donde solo encontraremos metodos que tiene que ver con la autenticacion de los usuarios.
    3. CarApi.dart. Donde encontramo los metodos para obtener informacion de los lavaderos de autos

### 2. UI.
dentro veremos todas las vistas de la aplicacion junto con widgets personalizados. Encontraremos las carpetas.
* Views
    1. details. Aqui encontraremos todos los archivos de vistas que requieran una vista "details" tales como:
    
        a. **carWash Detail**

        b. **services Detail**
        
    2. drawerOptions. Este fue un widget extra para manejar la vista del historial de servicios solicitados
* Widgets, aqui es donde estaran todos los widets personalizados que se reutilizaran a lo largo de la app, como botones, cards, entre otros.
