import 'package:carwash/ui/views/details/carWashDetails.dart';
import 'package:carwash/ui/views/details/serviceDetail.dart';
import 'package:carwash/ui/views/drawerOptions/historial.dart';
import 'package:carwash/ui/views/forgotPass.dart';
import 'package:carwash/ui/views/loginPage.dart';
import 'package:carwash/ui/views/profile.dart';
import 'package:carwash/ui/views/register.dart';
import 'package:carwash/ui/views/reviewsPage.dart';
import 'package:carwash/ui/views/writeReview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'ui/views/homePage.dart';

class Routes {
  static Route<dynamic> generateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        var params = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => HomePage(
                  userId: params,
                ));
      case 'login':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case 'register':
        return MaterialPageRoute(builder: (_) => RegisterPage());
      case 'forgot':
        return MaterialPageRoute(builder: (_) => ForgotPassPage());
      case 'car-details':
        var params = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => CarWashDetails(
                  lavadero: params,
                ));
      case 'service-details':
        var params = settings.arguments;
        return MaterialPageRoute(
            builder: (_) => ServiceDetail(
                  servicio: params,
                ));
      case 'historial':
        return MaterialPageRoute(builder: (_) => HistorialView());
      case 'profile':
        var params = settings.arguments;
        return MaterialPageRoute(builder: (_) => ProfileView(uid: params));
      case 'reviews':
        return MaterialPageRoute(builder: (_) => ReviewsPage());
      case 'write':
        return MaterialPageRoute(builder: (_) => WriteReview());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('Error, No se encontro ruta'),
                  ),
                ));
    }
  }
}

// class ScaleRoute extends PageRouteBuilder {
//   final Widget page;
//   ScaleRoute({this.page})
//       : super(
//             pageBuilder: (context, animation, secondAnimation) => page,
//             transitionsBuilder: (context, animation, secondAnimation, child) =>
//                 ScaleTransition(
//                   scale: Tween<double>(begin: 1, end: 1).animate(
//                       CurvedAnimation(
//                           parent: animation, curve: Curves.fastOutSlowIn)),
//                   child: child,
//                 ));
// }
