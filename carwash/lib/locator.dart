import 'package:carwash/core/controller/reviewsModel.dart';
import 'package:carwash/core/controller/servicioModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/services/api.dart';
import 'package:carwash/core/services/authServices.dart';
import 'package:carwash/core/services/carApi.dart';
import 'package:carwash/core/services/reviewsApi.dart';
import 'package:get_it/get_it.dart';

import 'core/controller/carModel.dart';

GetIt locator = GetIt.instance;

void setUpLocator() {
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => CarApi());
  locator.registerLazySingleton(() => ReviewsApi());
  locator.registerLazySingleton(() => AuthServices());
  locator.registerLazySingleton(() => UserModel());
  locator.registerLazySingleton(() => ServicesModel());
  locator.registerLazySingleton(() => CarModel());
  locator.registerLazySingleton(() => ReviewsModel());
}
