import 'package:carwash/core/services/authServices.dart';
import 'package:carwash/locator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './routes.dart';

void main() {
  setUpLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider(
      create: (_) => locator<AuthServices>().authController.stream,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData().copyWith(primaryColor: Colors.blue),
        onGenerateRoute: Routes.generateRoutes,
        initialRoute: 'login',
      ),
    );
  }
}
