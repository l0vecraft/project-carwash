import 'package:flutter/material.dart';

class AppStyle {
  static const List<Color> gradientColors = [
    Color(0xff42e0f5),
    Color(0xff7226ff)
  ];

  static const TextStyle loginText =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 25);

  static const TextStyle forgotPass =
      TextStyle(color: Colors.blueAccent, fontSize: 15);
  static const TextStyle buttonText =
      TextStyle(color: Colors.white, fontSize: 20);
  static const TextStyle subtitle = TextStyle(color: Colors.grey, fontSize: 15);
  static const TextStyle title1 = TextStyle(color: Colors.black, fontSize: 20);
  static const TextStyle title2 = TextStyle(color: Colors.white, fontSize: 20);
  static const TextStyle cardSubtitle =
      TextStyle(color: Colors.grey, fontSize: 12);
  static const TextStyle cardTitle =
      TextStyle(color: Colors.black, fontSize: 18);
  static const TextStyle cardTitle2 =
      TextStyle(color: Colors.white, fontSize: 20);
  static const TextStyle cardNumbers =
      TextStyle(color: Colors.white, fontSize: 50, fontWeight: FontWeight.bold);
  static const TextStyle cardTitle3 =
      TextStyle(color: Colors.black, fontSize: 30, fontWeight: FontWeight.bold);
  static const TextStyle cardSubtitle2 =
      TextStyle(color: Colors.grey, fontSize: 15);
  static const TextStyle cardPrice =
      TextStyle(color: Colors.blue, fontSize: 20, fontWeight: FontWeight.bold);
}
