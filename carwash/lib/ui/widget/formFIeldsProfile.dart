import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/models/users.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'customButton.dart';

class FormFieldsProfile extends StatelessWidget {
  final bool isEdit;
  final String uid;
  final User user;
  FormFieldsProfile({this.isEdit, this.user, this.uid});

  final _formKey = GlobalKey<FormState>();

  String _name = '';
  String _email = '';
  int _phone = 0;
  String _pass = '';

  @override
  Widget build(BuildContext context) {
    return BaseViewModel<UserModel>(
      builder: (context, model, child) => Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                TextFormField(
                  readOnly: !isEdit,
                  decoration: InputDecoration(
                      prefixIcon: Icon(
                        FontAwesomeIcons.user,
                      ),
                      labelText: isEdit ? 'Username' : '${user.username}',
                      hintText: '${user.username}'),
                  onSaved: (value) =>
                      value.isEmpty ? _name = user.username : _name = value,
                ),
                TextFormField(
                    readOnly: !isEdit,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        prefixIcon: Icon(
                          FontAwesomeIcons.envelope,
                        ),
                        labelText: isEdit ? 'Email' : '${user.email}',
                        hintText: '${user.email}'),
                    onSaved: (value) =>
                        value.isEmpty ? _email = user.email : _email = value),
                TextFormField(
                  readOnly: !isEdit,
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                      prefixIcon: Icon(FontAwesomeIcons.mobileAlt),
                      labelText: isEdit ? 'Telefono' : '+57 3984783817',
                      hintText: '+57 3984783817'),
                  onSaved: (value) => value.isEmpty
                      ? _phone = 3984783817
                      : _phone = int.parse(value),
                ),
                user.password == null ||
                        user.password.isEmpty ||
                        user.password.length == 0
                    ? Container()
                    : TextFormField(
                        obscureText: true,
                        readOnly: !isEdit,
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            FontAwesomeIcons.user,
                          ),
                          labelText: 'Password',
                          hintText: '${user.password}',
                        ),
                        onSaved: (value) => value.isEmpty
                            ? _pass = user.password
                            : _pass = value,
                      ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 15.0, horizontal: 15),
                  child: CustomButton(
                    text: 'Save Changes',
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        var data = User(
                            email: _email, password: _pass, username: _name);
                        Navigator.pushReplacementNamed(context, '/',
                            arguments: uid);
                        await model.updateUser(data, uid);
                        try {
                          // await Provider.of<UserController>(context)
                          //     .getActualUser();
                          // await Provider.of<UserController>(context).updateUser(
                          //     data,
                          //     Provider.of<UserController>(context)
                          //         .currentUser
                          //         .uid);
                        } catch (e) {
                          print(e);
                        }
                      }
                    },
                  ),
                )
              ],
            ),
          )),
    );
  }
}
