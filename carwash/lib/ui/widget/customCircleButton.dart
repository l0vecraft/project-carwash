import 'package:flutter/material.dart';

class CustomCircleButton extends StatelessWidget {
  final IconData icon;
  final Color color;
  final Color iconColor;
  final Function onPress;

  CustomCircleButton(
      {this.icon, this.onPress, this.color, this.iconColor = Colors.white});

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 25,
      backgroundColor: color,
      child: IconButton(
        iconSize: 25,
        icon: Icon(
          icon,
          color: iconColor,
        ),
        onPressed: onPress,
      ),
    );
  }
}
