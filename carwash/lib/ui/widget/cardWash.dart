import 'package:carwash/core/models/carwash.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:flutter/material.dart';

class CardWash extends StatelessWidget {
  final CarWash lavadero;
  CardWash({this.lavadero});

  _buildFirstPart() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              '${lavadero.dir}',
              overflow: TextOverflow.ellipsis,
              style: AppStyle.cardSubtitle,
            ),
            lavadero.capacity != 0
                ? Text(
                    'Puesto libre!',
                    style: TextStyle(color: Colors.green),
                  )
                : Text(
                    'Ocupado',
                    style: TextStyle(color: Colors.red),
                  )
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(right: 20.0),
          child: Text(
            '${lavadero.dis} Km',
            style: AppStyle.cardSubtitle,
          ),
        )
      ],
    );
  }

  _buildSecondPart() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 2),
          child: Text(
            '${lavadero.name}',
            style: AppStyle.cardTitle,
          ),
        ),
        Row(
          children: <Widget>[
            Text(
              '${lavadero.score.toString()} ',
              style: TextStyle(color: Colors.purple),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 18.0),
              child: Icon(
                Icons.star,
                color: Colors.purple,
              ),
            )
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 20),
      child: Card(
          elevation: 5,
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 4),
                  child: Icon(
                    Icons.local_car_wash,
                    color: Colors.blue,
                    size: 40,
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[_buildFirstPart(), _buildSecondPart()],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
