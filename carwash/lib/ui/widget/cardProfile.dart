import 'package:carwash/core/models/users.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:flutter/material.dart';

class CardProfile extends StatelessWidget {
  final User user;
  CardProfile({this.user});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 2,
        margin: EdgeInsets.only(left: 13, right: 13, top: 10),
        child: StreamBuilder(
          builder: (_, snap) => Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 18.0),
                child: InkWell(
                  onTap: () {},
                  child: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/user.png'),
                    radius: 45,
                  ),
                ),
              ),
              user.username == ''
                  ? Text('')
                  : Text(
                      '${user.username}',
                      style: AppStyle.cardTitle3,
                    ),
              SizedBox(
                height: 2,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: user.email == ''
                    ? Text('')
                    : Text(
                        '${user.email}',
                        style: AppStyle.cardSubtitle2,
                      ),
              ),
              Divider(
                height: 10,
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
        ));
  }
}
