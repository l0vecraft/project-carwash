import 'package:flutter/material.dart';

class CustomFormFields extends StatelessWidget {
  final String text;
  final bool isObscure;
  final Function validation;
  final Function onSave;

  CustomFormFields(
      {this.text, this.isObscure = false, this.validation, this.onSave});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, left: 30, right: 30),
      child: TextFormField(
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            hintText: text,
            contentPadding: EdgeInsets.only(left: 10, top: 15, bottom: 20)),
        obscureText: isObscure,
        validator: validation,
        onSaved: onSave,
      ),
    );
  }
}
