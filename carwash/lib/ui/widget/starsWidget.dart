import 'package:flutter/material.dart';

class StartsWidget extends StatelessWidget {
  final num stars;
  StartsWidget(this.stars);

  @override
  Widget build(BuildContext context) {
    /**
     * * este widget lo unico que hace es tomar las estrellas que les paso
     * * y comparar con un indice generado
     * * en caso de que las estrellas pasadas sean mayor que el indice(5) debera poner una estrella rellena
     * * en caso contrario una vacia ((NO DISTIGUE SI ES 1.5 o 1 POR LO QUE NO PONDRA ESTRELLAS MEDIAS))
     *  */
    return Row(
      children: List.generate(
          5,
          (index) => stars > index
              ? Icon(
                  Icons.star,
                  color: Colors.purple,
                )
              : Icon(
                  Icons.star_border,
                  color: Colors.purple,
                )),
    );
    // case 1:
    //   return Row(
    //     children: List.generate(1, (index) => Icon(Icons.star)).toList() +
    //         List.generate(4, (index) => Icon(Icons.star_border)).toList(),
    //   );
    // case 1:
    //   return Row(
    //     children: List.generate(1, (index) => Icon(Icons.star)).toList() +
    //         List.generate(4, (index) => Icon(Icons.star_border)).toList(),
    //   );
    // case 1:
    //   return Row(
    //     children: List.generate(1, (index) => Icon(Icons.star)).toList() +
    //         List.generate(4, (index) => Icon(Icons.star_border)).toList(),
    //   );
    // case 1:
    //   return Row(
    //     children: List.generate(1, (index) => Icon(Icons.star)).toList() +
    //         List.generate(4, (index) => Icon(Icons.star_border)).toList(),
    //   );
    // case 1:
    //   return Row(
    //     children: List.generate(1, (index) => Icon(Icons.star)).toList() +
    //         List.generate(4, (index) => Icon(Icons.star_border)).toList(),
    //   );
    // case 1:
    //   return Row(
    //     children: List.generate(1, (index) => Icon(Icons.star)).toList() +
    //         List.generate(4, (index) => Icon(Icons.star_border)).toList(),
    //   );
  }
}
