import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/models/users.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyDrawer extends StatefulWidget {
  final User user;
  final String uid;
  final UserModel model;

  MyDrawer({this.user, this.uid, this.model});

  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  var info = AboutListTile(
    child: Text('Info App'),
    applicationIcon: Icon(FontAwesomeIcons.android),
    applicationVersion: "v1.0.0",
    applicationName: 'Carwash demo',
    icon: Icon(Icons.info),
  );

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage('assets/images/user.png'),
            ),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: AppStyle.gradientColors,
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)),
            accountEmail:
                widget.user.email == null ? Text('') : Text(widget.user.email),
            accountName: widget.user.username == null
                ? Text('')
                : Text(widget.user.username),
          ),
          ListTile(
            title: Text('Home'),
            leading: Icon(Icons.home),
            onTap: () {
              Navigator.pushNamed(context, '/',
                  arguments: widget.uid == null ? '' : widget.uid);
            },
          ),
          ListTile(
            title: Text('Perfil'),
            leading: Icon(FontAwesomeIcons.userAlt),
            onTap: () {
              Navigator.pushNamed(context, 'profile',
                  arguments: widget.uid == null ? '' : widget.uid);
            },
          ),
          ListTile(
            title: Text('Historial'),
            leading: Icon(Icons.bookmark),
            onTap: () {
              Navigator.pushNamed(context, 'historial');
            },
          ),
          info,
          ListTile(
            title: Text('Eliminar cuenta'),
            leading: Icon(
              FontAwesomeIcons.trashAlt,
              color: Colors.red,
            ),
            onTap: () {
              showDialog(
                  context: context,
                  child: AlertDialog(
                    title: Text('Eliminar cuenta'),
                    content: Text('¿Esta seguro de querer borrar su cuenta?'),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('No'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      FlatButton(
                        child: Text('Si'),
                        color: Colors.red,
                        onPressed: () {
                          widget.model.removeUser(widget.uid);
                          widget.model.logOut();
                          // Provider.of<UserController>(context).removeUser(
                          //     Provider.of<UserController>(context)
                          //         .currentUser
                          //         .uid);
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ));
            },
          ),
          ListTile(
            title: Text('Salir'),
            leading: Icon(Icons.exit_to_app),
            onTap: () {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  'login', ModalRoute.withName('login'));
            },
          ),
        ],
      ),
    );
  }
}
