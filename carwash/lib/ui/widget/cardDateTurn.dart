import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/locator.dart';
import 'package:carwash/ui/views/notification.dart';
import 'package:flutter/material.dart';

import '../appstyle.dart';

class CardDateTurn extends StatelessWidget {
  final UserModel userServices = locator<UserModel>();

  // final List<Servicios> servicios;
  // CardDateTurn({this.servicios});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (_) => NotiPage()));
      },
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 12),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.only(left: 13.0, top: 8, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Tu turno es',
                      style: AppStyle.cardTitle3,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: CircleAvatar(
                        backgroundColor: Colors.purple,
                        child: Text(
                          '${userServices.data["turno"]}',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 4,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Fecha del servicio',
                      style: AppStyle.cardSubtitle2,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Text(
                        '${userServices.data["fecha"]}',
                        style: AppStyle.cardSubtitle2,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
