import 'package:flutter/material.dart';

class CustomListTile extends StatelessWidget {
  final String title;
  final String subtitle;
  final Widget trailing;
  final Widget leading;
  final bool isThreeline;
  CustomListTile(
      {this.title,
      this.subtitle,
      this.trailing,
      this.leading,
      this.isThreeline});
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: leading,
      title: Text(title),
      subtitle: Text(subtitle),
      trailing: trailing,
      isThreeLine: isThreeline,
    );
  }
}
