import 'package:carwash/ui/appstyle.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  CustomButton({this.text, this.onPressed});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Material(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Container(
          height: 45,
          width: 300,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              gradient: LinearGradient(
                  colors: AppStyle.gradientColors,
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight)),
          child: Center(
              child: Text(
            text,
            style: AppStyle.buttonText,
          )),
        ),
      ),
    );
  }
}
