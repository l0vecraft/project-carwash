import 'package:carwash/core/models/reviews.dart';
import 'package:carwash/ui/widget/starsWidget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CardReview extends StatelessWidget {
  final Reviews review;
  CardReview({this.review});

  Widget _buildTitle(Reviews reviews) {
    return Row(
      children: <Widget>[
        CircleAvatar(
          backgroundImage: AssetImage('assets/images/user.png'),
        ),
        SizedBox(
          width: 10,
        ),
        Text('${reviews.name}')
      ],
    );
  }

  Widget _buildSubtitle(Reviews reviews) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: <Widget>[
            StartsWidget(reviews.score),
            SizedBox(
              width: 5,
            ),
            Text(
                "${reviews.fecha.toDate().day}/${reviews.fecha.toDate().month}/${reviews.fecha.toDate().year}")
          ],
        ),
        Text('${reviews.description}'),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      isThreeLine: true,
      title: _buildTitle(review),
      subtitle: _buildSubtitle(review),
    );
  }
}
