import 'package:carwash/core/models/historial.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:flutter/material.dart';

class CardHistory extends StatefulWidget {
  final int price;
  CardHistory(this.price);
  @override
  _CardHistoryState createState() => _CardHistoryState();
}

class _CardHistoryState extends State<CardHistory> {
  int price = 0;

  _sumPrice() {
    for (var i in listaH) {
      price += i.price;
    }
  }

  @override
  void initState() {
    super.initState();
    _sumPrice();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 28.0, left: 10, right: 10),
      child: Material(
        elevation: 10,
        borderRadius: BorderRadius.circular(15),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                  colors: AppStyle.gradientColors,
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 28.0),
                child: Center(
                  child: Text(
                    'Available Balance',
                    style: TextStyle(fontSize: 22, color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 28.0),
                child: Center(
                  child: Text(
                    '\$ ${widget.price}',
                    style: TextStyle(
                        fontSize: 38,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
