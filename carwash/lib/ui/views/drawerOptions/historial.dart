import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/controller/carModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/models/carwash.dart';
import 'package:carwash/core/models/turn.dart';
import 'package:carwash/locator.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:carwash/ui/widget/cardHistory.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HistorialView extends StatelessWidget {
  List<Turn> _historial = [];
  List<CarWash> _carros = [];
  final CarModel _carModel = locator<CarModel>();
  int _total = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'Historial',
            style: TextStyle(fontSize: 22),
          ),
        ),
        body: BaseViewModel<UserModel>(
          onReadyModel: (model) {
            Future.delayed(Duration(milliseconds: 10), () async {
              _historial = await model.getHistory();
              for (var i in _historial) {
                _total += i.total;
                print(i.name);
              }
            });
          },
          builder: (context, model, child) => model.isLoading == ViewState.Busy
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                  children: <Widget>[
                    CardHistory(_total),
                    Padding(
                      padding: EdgeInsets.only(top: 15),
                      child: Center(
                        child: Text(
                          'See Activity',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        itemCount: _historial.length,
                        itemBuilder: (context, i) => ListTile(
                          leading: Icon(
                            FontAwesomeIcons.car,
                            color: Colors.blue,
                          ),
                          isThreeLine: true,
                          title: Text(
                            '${_historial[i].name}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                          subtitle: Text(
                              '${_historial[i].fecha.toDate().day}/${_historial[i].fecha.toDate().month}/${_historial[i].fecha.toDate().year}'),
                          trailing: Text(
                            '\$ ${_historial[i].total}',
                            style: TextStyle(
                                color: Colors.green,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
        ));
  }
}
