import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/controller/carModel.dart';
import 'package:carwash/core/models/carwash.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:carwash/ui/widget/cardWash.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<CarWash> _lavaderos = [];

  @override
  Widget build(BuildContext context) {
    return BaseViewModel<CarModel>(
        onReadyModel: (model) {
          //* este Future.delayed evita el error del setState o makeBuild
          //* le da tiempo de cargar a todo el build
          Future.delayed(Duration(milliseconds: 100), () async {
            _lavaderos = await model.getAllCarWash();
          });
        },
        builder: (context, modelo, child) => Container(
            color: Colors.white,
            child: modelo.isLoading == ViewState.Busy
                ? Center(child: CircularProgressIndicator())
                : ListView.builder(
                    itemCount: _lavaderos.length,
                    itemBuilder: (context, i) => InkWell(
                      onTap: () {
                        modelo.cid = _lavaderos[i].id;
                        Navigator.pushNamed(context, 'car-details',
                            arguments: _lavaderos[i]);
                      },
                      splashColor: Colors.black,
                      child: CardWash(
                        lavadero: _lavaderos[i],
                      ),
                    ),
                  )));
  }
}
