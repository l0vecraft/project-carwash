import 'package:carwash/core/controller/carModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/locator.dart';
import 'package:carwash/ui/widget/customFormFields.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class WriteReview extends StatefulWidget {
  @override
  _WriteReviewState createState() => _WriteReviewState();
}

class _WriteReviewState extends State<WriteReview> {
  final _formKey = GlobalKey<FormState>();
  bool _isScored = true;
  String _review = '';
  double _score = 0;
  final UserModel _userModel = locator<UserModel>();
  final CarModel _carWash = locator<CarModel>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: ListTile(
          title: Text(
            'Prueba titulo',
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          subtitle: Text(
            'Valora este lavadero',
            style: TextStyle(color: Colors.white60),
          ),
        ),
        actions: <Widget>[
          _isScored && _score != 0
              ? FlatButton(
                  highlightColor: Colors.purple[50],
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      Future.delayed(Duration(milliseconds: 100), () async {
                        _userModel.wirteReview(_score, _review,
                            _userModel.user.username, _carWash.cid);
                      });
                    }
                    Navigator.pop(context);
                  },
                  child: Text(
                    'Publicar',
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ))
              : Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Text(
                    'Publicar',
                    style: TextStyle(color: Colors.grey, fontSize: 18),
                  ),
                )
        ],
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 18.0),
              child: Center(
                child: SmoothStarRating(
                  size: 45,
                  allowHalfRating: false,
                  onRatingChanged: (value) {
                    setState(() {
                      _score = value;
                      print(_score);
                      _score > 0 ? _isScored = !_isScored : _isScored = false;
                    });
                  },
                  rating: _score,
                  filledIconData: Icons.star,
                  color: Colors.purple,
                  borderColor: Colors.purple,
                ),
              ),
            ),
            Form(
              key: _formKey,
              child: CustomFormFields(
                text: 'Describe tu experiencia aqui',
                onSave: (value) {
                  _review = value;
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
