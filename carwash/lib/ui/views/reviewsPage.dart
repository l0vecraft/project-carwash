import 'package:carwash/core/controller/reviewsModel.dart';
import 'package:carwash/core/models/reviews.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:carwash/ui/widget/cardReview.dart';
import 'package:carwash/ui/widget/starsWidget.dart';
import 'package:flutter/material.dart';

class ReviewsPage extends StatelessWidget {
  Widget _buildTitle(Reviews reviews) {
    return Row(
      children: <Widget>[
        CircleAvatar(
          backgroundImage: AssetImage('assets/images/user.png'),
        ),
        SizedBox(
          width: 10,
        ),
        Text('${reviews.name}')
      ],
    );
  }

  Widget _buildSubtitle(Reviews reviews) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            StartsWidget(reviews.score),
            SizedBox(
              width: 5,
            ),
            Text('${reviews.fecha}')
          ],
        ),
        Text('${reviews.description}'),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
          child: BaseViewModel<ReviewsModel>(
        onReadyModel: (model) {
          model.getAllReviews();
        },
        builder: (context, model, chil) => ListView.separated(
            itemCount: model.reviews.length,
            separatorBuilder: (context, i) => Divider(),
            itemBuilder: (context, i) => CardReview(
                  review: model.reviews[i],
                )),
      )),
    );
  }
}
