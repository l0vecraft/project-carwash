import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/models/users.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:carwash/ui/widget/customButton.dart';
import 'package:carwash/ui/widget/customCircleButton.dart';
import 'package:carwash/ui/widget/customFormFields.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();

  String _user;

  String _email;

  String _pass;

  _buildFields() {
    return Card(
      child: Container(
        child: Column(
          children: <Widget>[
            CustomFormFields(
              text: 'Username',
              validation: (value) => value.length <= 3 || value.isEmpty
                  ? 'The username must have 3 or more characters'
                  : null,
              onSave: (value) {
                _user = value;
                print(_user);
              },
            ),
            CustomFormFields(
              text: 'Email',
              validation: (String value) =>
                  value.length <= 0 || !value.contains('@')
                      ? 'Invalid Email'
                      : null,
              onSave: (value) {
                _email = value;
              },
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 13.0),
              child: CustomFormFields(
                text: 'Password',
                isObscure: true,
                validation: (value) => value.length < 8 || value.length == 0
                    ? 'Password must have 8 characters'
                    : null,
                onSave: (value) {
                  _pass = value;
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildRegister(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 90.0),
      child: FlatButton(
        child: RichText(
          text: TextSpan(children: [
            TextSpan(
                text: 'Have account? ', style: TextStyle(color: Colors.black)),
            TextSpan(text: 'Sign In', style: AppStyle.forgotPass)
          ]),
        ),
        onPressed: () {
          Navigator.pushNamed(context, 'login');
        },
      ),
    );
  }

  _buildSocialButtons(UserModel model) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CustomCircleButton(
            icon: FontAwesomeIcons.google,
            color: Colors.red,
            onPress: () async {
              Future.delayed(Duration(milliseconds: 100), () async {
                var log = await model.addGoogleUser();
                Navigator.pushNamed(context, '/', arguments: log.uid);
              });
            },
          ),
          SizedBox(
            width: 20,
          ),
          CustomCircleButton(
            icon: FontAwesomeIcons.facebook,
            color: Colors.blue,
            onPress: () async {
              Future.delayed(Duration(milliseconds: 100), () async {
                var log = await model.addFacebookuser();
                Navigator.pushNamed(context, '/', arguments: log.uid);
              });
            },
          ),
        ],
      ),
    );
  }

  _buildButton(BuildContext context, UserModel model) {
    return model.isLoading == ViewState.Busy
        ? Center(
            child: CircularProgressIndicator(),
          )
        : CustomButton(
            text: 'Sign Up',
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                var data = User(
                    username: _user, email: _email, password: _pass, turno: 0);
                try {
                  Future.delayed(Duration(milliseconds: 100), () async {
                    await model.addUser(data);
                  });
                  Navigator.pushReplacementNamed(context, 'login');
                } catch (e) {
                  print(e);
                }
              }
            });
  }

  @override
  Widget build(BuildContext context) {
    return BaseViewModel<UserModel>(
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: ListView(
            children: <Widget>[
              Hero(
                tag: 'hero',
                child: Image.asset(
                  'assets/images/car_wash_logo.jpg',
                  scale: 4.0,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: Text(
                  'Register',
                  style: AppStyle.loginText,
                ),
              ),
              SizedBox(height: 2),
              Form(
                key: _formKey,
                child: _buildFields(),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 18, vertical: 8),
                  child: _buildButton(context, model)),
              _buildSocialButtons(model),
              _buildRegister(context),
            ],
          ),
        ),
      ),
    );
  }
}
