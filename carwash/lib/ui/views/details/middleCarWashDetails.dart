import 'package:carwash/core/models/reviews.dart';
import 'package:carwash/ui/widget/cardReview.dart';
import 'package:flutter/material.dart';

class MiddleCarWashDetails extends StatefulWidget {
  final List<Reviews> reviewsModel;
  double maxHeigth;
  MiddleCarWashDetails({@required this.reviewsModel, @required this.maxHeigth});
  @override
  _MiddleCarWashDetailsState createState() => _MiddleCarWashDetailsState();
}

class _MiddleCarWashDetailsState extends State<MiddleCarWashDetails> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        height: widget.maxHeigth * 0.3,
        child: Column(
          children: <Widget>[
            CardReview(
                review: widget.reviewsModel[widget.reviewsModel.length - 2]),
            CardReview(
                review: widget.reviewsModel[widget.reviewsModel.length - 1]),
          ],
        ),
      ),
    );
  }
}

class CustomFlatButton extends StatelessWidget {
  final String text;
  final Function onPress;
  const CustomFlatButton({@required this.text, this.onPress});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5.0),
      child: FlatButton(
          highlightColor: Colors.purple[50],
          onPressed: onPress,
          child: Text(
            text,
            style: TextStyle(color: Colors.purple, fontSize: 18),
          )),
    );
  }
}
