import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/controller/carModel.dart';
import 'package:carwash/core/controller/reviewsModel.dart';
import 'package:carwash/core/controller/servicioModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/models/carwash.dart';
import 'package:carwash/core/models/reviews.dart';
import 'package:carwash/core/models/servicios.dart';
import 'package:carwash/locator.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:carwash/ui/views/details/middleCarWashDetails.dart';
import 'package:carwash/ui/widget/cardDateTurn.dart';
import 'package:carwash/ui/widget/starsWidget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CarWashDetails extends StatefulWidget {
  final CarWash lavadero;

  CarWashDetails({this.lavadero});

  @override
  _CarWashDetailsState createState() => _CarWashDetailsState();
}

class _CarWashDetailsState extends State<CarWashDetails> {
  final UserModel userServices = locator<UserModel>();
  final ReviewsModel _reviewsModel = locator<ReviewsModel>();
  String _resultDate = '';
  List<Reviews> _reviewsList = [];

  ScrollController _scrollController;

  _buildMyTurn(String text, int turno, Color color, List<Servicios> serv) {
    if (userServices.data['fecha'] != _resultDate) {
      return CardDateTurn();
    }
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Card(
        elevation: 5,
        color: color,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 18.0, horizontal: 65),
          child: Column(
            children: <Widget>[
              Text(
                '$text',
                style: AppStyle.cardTitle2,
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                '$turno',
                style: AppStyle.cardNumbers,
              )
            ],
          ),
        ),
      ),
    );
  }

  void _getReviews() async {
    await _reviewsModel.getScore();
    _reviewsList.addAll(await _reviewsModel.getAllReviews());
  }

  _buildCadService(
      BuildContext context, ServicesModel servicio, Servicios serv) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, 'service-details', arguments: serv);
      },
      onLongPress: () {
        servicio.cid = widget.lavadero.id;
        servicio.addServicio(serv);
        servicio.subTotal();
        //aqui se crea un scnackbar
        final snack = SnackBar(
          content: Text('articulo agregado ${servicio.addedServices.length}'),
        );
        //y se llama en el mismo cuando se presione
        Scaffold.of(context).showSnackBar(snack);
      },
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 12),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.only(left: 13.0, top: 8, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  '${serv.name}',
                  style: AppStyle.cardTitle3,
                ),
                Text(
                  '${serv.description}',
                  overflow: TextOverflow.ellipsis,
                  style: AppStyle.cardSubtitle2,
                ),
                Divider(),
                Row(
                  children: <Widget>[
                    Text(
                      '\$${serv.price}',
                      style: AppStyle.cardPrice,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _getDate() {
    DateFormat _dateFormat = DateFormat('dd-MM-yyyy');

    DateTime _selectedDate = DateTime.now();
    setState(() {
      _resultDate = _dateFormat.format(_selectedDate);
    });
  }

  Widget _buildFloatButton() {
    //? el top margin por defecto en el positioned, -4 es aparentemente la alineacion exacta
    double defaultTop = 165.0 - 4.0;
    //pixeles del top donde el escalado empezara
    double scaleStart = 96.0;
    //pixeles del top donde el escalado terminara
    double scaleEnd = scaleStart / 2;
    double scale = 1.0;
    double top = defaultTop;
    if (_scrollController.hasClients) {
      //* en caso de que se empiece a mover
      double offset =
          _scrollController.offset; //* empezara a restarse, yendo hacia arriba
      top -= offset;

      if (offset < defaultTop - scaleStart) {
        //* si el offset es mas pequeño, entonces que no reduzca mas
        scale = 1.0;
      } else if (offset < defaultTop - scaleEnd) {
        //* si aparentemente esta entre el scalestart y el end , puede seguir bajando
        scale = (defaultTop - scaleEnd - offset) / scaleEnd;
      } else {
        scale = 0.0;
      }
    }
    return Positioned(
      top: top,
      right: 20,
      child: Transform(
        transform: Matrix4.identity()..scale(scale),
        alignment: Alignment.center,
        child: FloatingActionButton(
            backgroundColor: Colors.purple,
            child: Icon(Icons.line_style),
            onPressed: () {
              Navigator.pushNamed(context, 'reviews');
            }),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    //*para personalizar el scroll y el floatingButton
    _scrollController = ScrollController();
    _scrollController.addListener(() => setState(() {}));
    _getDate();
    _getReviews();
  }

  @override
  void dispose() {
    super.dispose();
    //* y que se quite cuando sea necesario
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      double maxHeigth = constraints.maxHeight;
      double maxWidth = constraints.maxWidth;
      return Scaffold(
          body: Stack(
        children: <Widget>[
          CustomScrollView(
            controller:
                _scrollController, //* el controller hara que todo suba y baje igual
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                expandedHeight: 190,
                flexibleSpace: FlexibleSpaceBar(
                  background: Image.asset(
                    'assets/images/logo2.jpg',
                    fit: BoxFit.fill,
                  ),
                  title: Text(
                    '${widget.lavadero.name}',
                    style: AppStyle.title2,
                  ),
                ),
              ),
              SliverList(
                  delegate: SliverChildListDelegate([
                Container(
                    height: maxHeigth,
                    child: BaseViewModel<CarModel>(
                      builder: (context, car, child) {
                        return BaseViewModel<ServicesModel>(
                          onReadyModel: (model) {
                            Future.delayed(Duration(milliseconds: 100),
                                () async {
                              await model.newGetServices(widget.lavadero.id);
                            });
                          },
                          builder: (context, model, child) => model.isLoading ==
                                  ViewState.Busy
                              ? Center(
                                  child: CircularProgressIndicator(),
                                )
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    userServices.data['turno'] != 0 &&
                                            userServices.data['turno'] !=
                                                null &&
                                            userServices.data["cid"] == car.cid
                                        ? _buildMyTurn(
                                            'Tu turno ',
                                            userServices.data["turno"],
                                            Colors.orange,
                                            model.servicios)
                                        : Container(),
                                    Divider(),
                                    _CalificacionesCard(
                                        reviewsModel: _reviewsModel),
                                    CustomFlatButton(
                                      text: 'Escribe una',
                                      onPress: () {
                                        Navigator.of(context)
                                            .pushNamed('write');
                                      },
                                    ),
                                    MiddleCarWashDetails(
                                      reviewsModel: _reviewsList,
                                      maxHeigth: maxHeigth,
                                    ),
                                    CustomFlatButton(
                                      text: 'Ver todas',
                                      onPress: () {
                                        Navigator.pushNamed(context, 'reviews');
                                      },
                                    ),
                                    Expanded(
                                        child: model.length != 0 ||
                                                model.length != null
                                            ? ListView.builder(
                                                itemCount: model.length,
                                                itemBuilder: (context, i) =>
                                                    _buildCadService(
                                                        context,
                                                        model,
                                                        model.servicios[i]))
                                            : Center(
                                                child: Text(
                                                    'Este lavadero aun no tiene servicios')))
                                  ],
                                ),
                        );
                      },
                    ))
              ]))
            ],
          ),
          _buildFloatButton()
        ],
      ));
    });
  }
}

class _CalificacionesCard extends StatelessWidget {
  const _CalificacionesCard({
    Key key,
    @required ReviewsModel reviewsModel,
  })  : _reviewsModel = reviewsModel,
        super(key: key);

  final ReviewsModel _reviewsModel;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'calificaciones y opiniones',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(
              height: 2,
            ),
            Text(
              '${_reviewsModel.score}',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 130.0, top: 10),
              child: StartsWidget(_reviewsModel.score),
            )
          ],
        ),
      ),
    );
  }
}
