import 'package:carwash/core/controller/servicioModel.dart';
import 'package:carwash/core/models/servicios.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:flutter/material.dart';

class ServiceDetail extends StatelessWidget {
  final Servicios servicio;
  ServiceDetail({this.servicio});

  _buildFloatButton(BuildContext context, Servicios s) {
    return BaseViewModel<ServicesModel>(
      builder: (context, services, child) {
        return FloatingActionButton(
          onPressed: () {
            services.addServicio(s);
            services.subTotal();
          },
          elevation: 8,
          backgroundColor: Colors.purple,
          tooltip: 'agregar',
          child: Icon(Icons.add),
        );
      },
    );
  }

  _buildCustomIcons(IconData icon, String text) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 10),
      child: Row(
        children: <Widget>[
          Icon(
            icon,
            color: Colors.blue,
            size: 30,
          ),
          Text(
            text,
            textScaleFactor: 1.8,
            style: AppStyle.cardSubtitle,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: false,
            expandedHeight: 190,
            flexibleSpace: FlexibleSpaceBar(
              titlePadding: EdgeInsets.only(left: 15, bottom: 8),
              title: Text(
                '${servicio.name}',
                textAlign: TextAlign.end,
                softWrap: true,
                textScaleFactor: 1,
                style: AppStyle.title2,
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              Row(
                children: <Widget>[
                  _buildCustomIcons(
                      Icons.attach_money, '${servicio.price} USD'),
                  _buildCustomIcons(Icons.timelapse, '${servicio.duration} min')
                ],
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Text(
                  '${servicio.description}',
                  textScaleFactor: 1.7,
                  style: AppStyle.subtitle,
                ),
              ),
            ]),
          )
        ],
      ),
      floatingActionButton: _buildFloatButton(context, servicio),
    );
  }
}
