import 'package:carwash/core/models/carwash.dart';
import 'package:flutter/material.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapsPage extends StatefulWidget {
  final List<CarWash> lavaderos;
  MapsPage({this.lavaderos});
  @override
  _MapsPageState createState() => _MapsPageState();
}

class _MapsPageState extends State<MapsPage> {
  final LatLng addrs = LatLng(10.3997200, -75.5144400);
  //List<CarWash> lavaderos = [];
  GoogleMapController mapController;

  @override
  void initState() {
    super.initState();
    //_getData();
  }

  void _onMapCreate(GoogleMapController controller) {
    mapController = controller;
    _centerView();
  }

  _centerView() async {
    //? se supone que esto es para que el mapa cargue correctamente
    //? a medida que vamos haciendo scroll y moviendonos
    await mapController.getVisibleRegion();

    // var cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 50);
    // mapController.animateCamera(cameraUpdate);//esta es la transicion
  }

  Set<Marker> _createMarkers() {
    var tmp = Set<Marker>();
    for (var l in widget.lavaderos) {
      tmp.add(Marker(
        markerId: MarkerId(l.name),
        position: LatLng(l.location.latitude, l.location.longitude),
        infoWindow: InfoWindow(title: l.name),
      ));
    }
    return tmp;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: GoogleMap(
          onMapCreated: _onMapCreate,
          initialCameraPosition: CameraPosition(target: addrs, zoom: 16),
          myLocationButtonEnabled: true,
          myLocationEnabled: true,
          mapToolbarEnabled: true,
          markers: _createMarkers(),
        ));
  }
}
