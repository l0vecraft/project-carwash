import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/services/api.dart';
import 'package:carwash/locator.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:carwash/ui/widget/customButton.dart';
import 'package:carwash/ui/widget/customCircleButton.dart';
import 'package:carwash/ui/widget/customFormFields.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formState = GlobalKey<FormState>();
  bool _isCheck = false;

  String _user;

  String _pass;

  _buildCard(BuildContext context) {
    return Card(
      child: Form(
        key: _formState,
        child: Column(
          children: <Widget>[
            CustomFormFields(
                text: 'Email',
                validation: (value) =>
                    value.length == 0 ? 'Invalid username' : null,
                onSave: (value) {
                  _user = value;
                  print(_user);
                }),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: CustomFormFields(
                  text: 'Password',
                  isObscure: true,
                  //TODO: hacer las respectivas validaciones de la passwd
                  validation: (value) =>
                      value.length == 0 ? 'Invalid Password' : null,
                  onSave: (value) {
                    _pass = value;
                    print(_pass);
                  }),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Row(
                        children: <Widget>[
                          Checkbox(
                            onChanged: (value) {
                              setState(() {
                                _isCheck = !_isCheck;
                              });
                            },
                            value: _isCheck,
                          ),
                          Text(
                            'Remember me',
                            style: AppStyle.subtitle,
                          )
                        ],
                      )),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, 'forgot');
                    },
                    child: Text(
                      'Forgot Password?',
                      style: AppStyle.forgotPass,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildRegister(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 100.0),
      child: FlatButton(
        child: RichText(
          text: TextSpan(children: [
            TextSpan(text: 'New user? ', style: TextStyle(color: Colors.black)),
            TextSpan(text: 'Sign Up', style: AppStyle.forgotPass)
          ]),
        ),
        onPressed: () {
          Navigator.pushNamed(context, 'register');
        },
      ),
    );
  }

  _buildValidation(BuildContext context, UserModel model) async {
    if (_formState.currentState.validate()) {
      _formState.currentState.save();
      Future.delayed(Duration(milliseconds: 100), () async {
        FirebaseUser log = await model.login(_user, _pass);
        model.uid = log.uid;
        Navigator.pushReplacementNamed(context, '/', arguments: log.uid);
      });
    }
  }

  _buildSocialButtons(UserModel model) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CustomCircleButton(
            icon: FontAwesomeIcons.google,
            color: Colors.red,
            onPress: () {
              Future.delayed(Duration(milliseconds: 100), () async {
                FirebaseUser log = await model.loginGoogle();
                if (model.state == ViewState.LogIn) {
                  Navigator.pushReplacementNamed(context, '/',
                      arguments: log.uid);
                }
              });
              // Provider.of<UserController>(context).loginCredentials();
              // if (Provider.of<UserController>(context).isLogged) {
              //   Navigator.pushNamed(context, 'login');
              // }
            },
          ),
          SizedBox(
            width: 20,
          ),
          CustomCircleButton(
            icon: FontAwesomeIcons.facebook,
            color: Colors.blue,
            onPress: () async {
              // await locator<Api>().facebookLogin();
              FirebaseUser log = await model.loginFacebook();
              if (model.state == ViewState.LogIn) {
                Navigator.pushReplacementNamed(context, '/',
                    arguments: log.uid);
              }
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseViewModel<UserModel>(
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: model.isLoading == ViewState.Busy
              ? Center(child: CircularProgressIndicator())
              : ListView(
                  children: <Widget>[
                    Hero(
                      tag: 'logo',
                      child: Image.asset(
                        'assets/images/car_wash_logo.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 48.0,
                      ),
                      child: Text(
                        'Login',
                        style: AppStyle.loginText,
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                          top: 4.0,
                        ),
                        child: _buildCard(context)),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 7),
                      child: CustomButton(
                        text: 'Sign in',
                        onPressed: () async {
                          _buildValidation(context, model);

                          // Provider.of<UserController>(context)
                          //     .login(_user, _pass);
                          // if (Provider.of<UserController>(context).isLogged) {
                          //   Navigator.pushNamed(context, '/');
                          // }
                        },
                      ),
                    ),
                    _buildSocialButtons(model),
                    _buildRegister(context)
                  ],
                ),
        ),
      ),
    );
  }
}
