import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/models/users.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:carwash/ui/widget/cardProfile.dart';
import 'package:carwash/ui/widget/formFIeldsProfile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ProfileView extends StatefulWidget {
  final String uid;
  ProfileView({this.uid});
  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  bool _isEdit = false;
  User _user = User();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Profile',
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: _isEdit
                ? Icon(FontAwesomeIcons.times)
                : Icon(FontAwesomeIcons.pencilAlt),
            iconSize: 20,
            onPressed: () {
              setState(() {
                _isEdit = !_isEdit;
              });
            },
          )
        ],
      ),
      body: BaseViewModel<UserModel>(onReadyModel: (model) {
        Future.delayed(Duration(milliseconds: 10), () async {
          _user = await model.getUserById(widget.uid);
        });
      }, builder: (context, model, child) {
        if (model.isLoading == ViewState.Busy) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return Container(
              child: ListView(
            children: <Widget>[
              CardProfile(user: _user == null ? User() : _user),
              Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: FormFieldsProfile(
                      isEdit: _isEdit,
                      user: _user == null ? User() : _user,
                      uid: widget.uid == null ? '' : widget.uid)),
            ],
          )
              // child:
              );
        }
      }),
    );
  }
}
