import 'package:carwash/core/controller/carModel.dart';
import 'package:carwash/core/controller/servicioModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/models/carwash.dart';
import 'package:carwash/core/models/users.dart';
import 'package:carwash/locator.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:carwash/ui/views/mainPage.dart';
import 'package:carwash/ui/views/mapsPage.dart';
import 'package:carwash/ui/widget/myDrawer.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'notification.dart';

class HomePage extends StatefulWidget {
  final String userId;

  HomePage({this.userId});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  User _user;
  List<CarWash> _carwashes = [];
  List _results = [];

  TabController _tabController;
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    // _getCurrentUser();
    _getCarwash();
    _tabController =
        TabController(length: 3, vsync: this, initialIndex: _currentIndex);
  }

  _getCarwash() async {
    _carwashes.addAll(await locator<CarModel>().getAllCarWash());
  }

  _buildIcons(IconData icon) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Icon(icon, size: 35),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseViewModel<ServicesModel>(builder: (context, services, child) {
      return BaseViewModel<UserModel>(
        onReadyModel: (model) {
          Future.delayed(Duration(milliseconds: 100), () async {
            _user = await model.getUserById(widget.userId);
            model.data['uid'] = widget.userId;
          });
        },
        builder: (context, model, child) => Scaffold(
            appBar: AppBar(
              title: Text('CarWash'),
            ),
            backgroundColor: Colors.blue,
            bottomNavigationBar: TabBar(
              indicatorColor: Colors.white,
              unselectedLabelColor: Colors.grey,
              controller: _tabController,
              onTap: (index) {
                if (index == 2) {
                  services.clearNotification();
                }
              },
              tabs: <Widget>[
                _buildIcons(Icons.home),
                _buildIcons(Icons.map),
                services.noti == 0
                    ? _buildIcons(Icons.notifications)
                    : Badge(
                        badgeColor: Colors.red,
                        elevation: 3,
                        badgeContent: Text(
                          '${services.noti}',
                          style: TextStyle(color: Colors.white),
                        ),
                        child: _buildIcons(Icons.notifications),
                      )
              ],
            ),
            drawer: MyDrawer(
              user: _user,
              uid: widget.userId,
              model: model,
            ),
            body: TabBarView(
              controller: _tabController,
              //* las fisicas es para cuando este en la pestaña Maps
              //* no se ande cambiando
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                MainPage(),
                MapsPage(lavaderos: _carwashes),
                NotiPage()
              ],
            )),
      );
    });
  }
}
