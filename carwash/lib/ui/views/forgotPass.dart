import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:carwash/ui/widget/customButton.dart';
import 'package:flutter/material.dart';

class ForgotPassPage extends StatefulWidget {
  @override
  _ForgotPassPageState createState() => _ForgotPassPageState();
}

class _ForgotPassPageState extends State<ForgotPassPage> {
  final _formKey = GlobalKey<FormState>();

  String _email;

  _buildContent(BuildContext context, UserModel model) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Text(
            'Reset Password',
            style: AppStyle.loginText,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            'Enter your Email for Change your password',
            style: AppStyle.subtitle,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Text(
            'Forget Password',
            style: AppStyle.title1,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          child: Form(
            key: _formKey,
            child: TextFormField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  hintText: 'Enter Email',
                  contentPadding:
                      EdgeInsets.only(left: 10, top: 20, bottom: 20)),
              validator: (value) =>
                  !value.contains('@') || value.isEmpty || value.length == 0
                      ? "Invalid email"
                      : null,
              onSaved: (value) {
                _email = value;
              },
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 30),
          child: CustomButton(
            text: 'Reset',
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                model.forgetPassword(_email);
                // Navigator.of(context).pushNamed('login');
                showDialog(
                  context: context,
                  child: AlertDialog(
                    content: Text(
                        'Por favor, revise su correo y siga las instrucciones'),
                    title: Text('Forget Password'),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('OK'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      )
                    ],
                  ),
                );
              }
            },
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseViewModel<UserModel>(
      builder: (context, model, child) {
        if (model.isLoading == ViewState.Busy) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return Scaffold(
            body: Stack(
              children: <Widget>[
                AppBar(
                  title: Text('Forgot Password'),
                ),
                Positioned(
                  bottom: 0.0,
                  child: Container(
                      height: 600,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20)),
                          color: Colors.white),
                      child: _buildContent(context, model)),
                )
              ],
            ),
          );
        }
      },
    );
  }
}
