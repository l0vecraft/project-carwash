import 'package:carwash/core/controller/carModel.dart';
import 'package:carwash/core/controller/servicioModel.dart';
import 'package:carwash/core/controller/userModel.dart';
import 'package:carwash/core/models/carwash.dart';
import 'package:carwash/core/models/servicios.dart';
import 'package:carwash/locator.dart';
import 'package:carwash/ui/views/baseViewModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:carwash/ui/appstyle.dart';
import 'package:carwash/ui/widget/customButton.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NotiPage extends StatefulWidget {
  @override
  _NotiPageState createState() => _NotiPageState();
}

class _NotiPageState extends State<NotiPage> {
  final carServices = locator<CarModel>();
  bool _isSave = false;
  final userServices = locator<UserModel>();
  CarWash _carWash = CarWash();
  DateTime _selectedDate = DateTime.now();

  _showDate(BuildContext context) async {
    return showDatePicker(
        context: context,
        initialDate: DateTime.now().add(
          Duration(seconds: 1),
        ),
        firstDate: DateTime.now(),
        lastDate: DateTime(2100));
  }

  _showTime(BuildContext context) {
    final hora = DateTime.now();
    return showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: hora.hour, minute: hora.minute),
    );
  }

  _buildReservButton(CarModel car, ServicesModel services) {
    return Flexible(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 26, vertical: 40),
        child: Center(
          child: CustomButton(
            text: 'Apartar turno',
            onPressed: () async {
              var _date = await _showDate(context);
              if (_date == null) return;
              var _time = await _showTime(context);
              if (_time == null) return;
              setState(() {
                _selectedDate = DateTime(_date.year, _date.month, _date.day,
                    _time.hour, _time.minute);
                _isSave = !_isSave;
              });
              userServices.currentUserId();
              //* esto es para cuando se asigne la cita
              userServices.data["uid"] = userServices.uid;
              userServices.data["cid"] = car.cid;
              userServices.data['name'] = _carWash.name;
              userServices.data["fecha"] = Timestamp.fromMillisecondsSinceEpoch(
                  _selectedDate.millisecondsSinceEpoch);
              userServices.data['services'] =
                  services.addedServices.map((Servicios s) => s.name).toList();
              userServices.data["total"] = services.total;

              userServices.idTurno = await locator<UserModel>()
                  .getTurn(userServices.data, _carWash);
            },
          ),
        ),
      ),
    );
  }

  _buildCancelButton() {
    return Flexible(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 26, vertical: 40),
        child: Center(
          child: RaisedButton(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 11.0, horizontal: 60),
              child: Text(
                'Cancelar turno',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 22),
              ),
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: Colors.red,
            onPressed: () {
              setState(() {
                _isSave = !_isSave;
              });
              userServices.cancelTurno(userServices.data, userServices.idTurno);
            },
          ),
        ),
      ),
    );
  }

  _buildContent(ServicesModel services, CarModel car, AsyncSnapshot snap) {
    return Card(
      borderOnForeground: true,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: ListView.builder(
              itemCount: services.addedServices.length,
              itemBuilder: (context, i) => ListTile(
                isThreeLine: true,
                title: Text(
                  '${services.addedServices[i].name}',
                ),
                subtitle: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('${services.addedServices[i].duration} min'),
                    Text(
                      '\$${services.addedServices[i].price}',
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                trailing: _isSave
                    ? IconButton(
                        icon: Icon(
                          FontAwesomeIcons.times,
                          color: Colors.grey[300],
                        ),
                        onPressed: () {},
                      )
                    : IconButton(
                        icon: Icon(FontAwesomeIcons.times),
                        onPressed: () {
                          setState(() {
                            services.deleteItem(i);
                          });
                        },
                      ),
              ),
            ),
          ),
          Divider(),
          Expanded(
            flex: -1,
            child: Padding(
              padding: const EdgeInsets.only(
                left: 12.0,
                top: 10,
              ),
              child: Text(
                'Total: \$${services.total}',
                style: AppStyle.loginText,
              ),
            ),
          ),
          snap.data == null
              ? _buildReservButton(car, services)
              : _buildCancelButton()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseViewModel<ServicesModel>(
      builder: (context, services, child) {
        return Container(
            color: Colors.white,
            child: BaseViewModel<CarModel>(
                onReadyModel: (model) {
                  Future.delayed(Duration(milliseconds: 100), () async {
                    _carWash = await model.getCarById(model.cid);
                  });
                },
                builder: (context, car, child) => StreamBuilder(
                      stream: userServices
                          .getTurnState(userServices.data['uid'])
                          .asStream(),
                      builder: (context, AsyncSnapshot snap) {
                        if (!snap.hasData &&
                            services.addedServices.length == 0) {
                          return Center(
                            child: Text('Aun no has añadido servicios'),
                          );
                        } else {
                          // return ListView.builder(
                          //   itemCount: snap.data.length,
                          //   itemBuilder: (_, i) => ListTile(
                          //     title: Text("${snap.data[i]['fecha']}"),
                          //   ),
                          // );
                          if (services.addedServices.length == 0) {
                            return Center(
                              child: Text('Aun no has añadido servicios'),
                            );
                          }
                          // if (snap.data.turno == null) {
                          //   userServices.data['turno'] = 1;
                          // }
                          // print(snap.data.tipo);
                        }
                        return _buildContent(services, car, snap);
                      },
                    )));
        // : Center(
        //     child: Text('Aun no has añadido servicios'),
        //   ));
      },
    );
  }
}
