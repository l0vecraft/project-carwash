import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/models/reviews.dart';
import 'package:carwash/core/services/reviewsApi.dart';
import 'package:carwash/locator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ReviewsModel extends BaseModel {
  ReviewsApi _api = locator<ReviewsApi>();
  List<Reviews> _reviews = [];
  double _score = 0;

  get reviews => _reviews;
  get score => _score;

  Future getAllReviews() async {
    _reviews.clear();
    isLoading = ViewState.Busy;
    QuerySnapshot result = await _api.getAllComments();
    if (result != null && result.documents.length != 0) {
      _reviews.addAll(listReviews);
      _reviews.addAll(result.documents.map((e) => Reviews.fromJson(e.data)));
    }
    isLoading = ViewState.Idle;
    return _reviews;
  }

  Future getScore() {
    double puntaje = 0;
    isLoading = ViewState.Busy;
    for (var r in listReviews) {
      puntaje += (r.score / listReviews.length);
    }
    _score = double.parse(puntaje.toStringAsFixed(1));
    isLoading = ViewState.Idle;
  }

  //TODO:Editar comentarios
  //TODO: eliminar comentario
}
