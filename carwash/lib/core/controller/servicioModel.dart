import 'package:carwash/core/controller/baseModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/servicios.dart';

class ServicesModel extends BaseModel {
  //prueba
  String _cid = '';
  //prueba
  Firestore _db = Firestore.instance;
  List<Servicios> _servicios = [];
  List<Servicios> _addedServices = [];
  int _not = 0;
  int _total = 0;
  bool _hasServices = false;

  get length => _servicios.length;
  get servicios => _servicios;
  get addedServices => _addedServices;
  get cid => _cid;

  get noti => _not;
  get total => _total;

  set cid(newValue) => _cid = newValue;

  Future newGetServices(String cid) async {
    isLoading = ViewState.Busy;
    _servicios.clear();
    try {
      var result = await _db
          .collection('services')
          .document(cid)
          .collection('myServices')
          .getDocuments();
      for (var s in result.documents) {
        _servicios.add(Servicios.fromMap(s.data, s.documentID));
      }
      print(_servicios[0]);
      isLoading = ViewState.Idle;
      return _servicios;
    } catch (e) {
      print(e);
    }
  }

  void addServicio(Servicios s) {
    isLoading = ViewState.Busy;

    _addedServices.add(s);
    _not++;
    _hasServices = true;
    isLoading = ViewState.Idle;
  }

  void clearNotification() {
    isLoading = ViewState.Busy;
    _not = 0;
    isLoading = ViewState.Idle;
  }

  int subTotal() {
    isLoading = ViewState.Busy;
    if (_addedServices.length != 0) {
      _total = 0;
      for (var s in _addedServices) {
        _total += s.price;
      }
    } else {
      _total = 0;
    }
    isLoading = ViewState.Idle;
    return _total;
  }

  void deleteItem(int i) {
    isLoading = ViewState.Busy;
    _addedServices.removeAt(i);
    subTotal();
    isLoading = ViewState.Idle;
  }
}
