import 'package:flutter/widgets.dart';

enum ViewState { LogIn, LogOut, Busy, Idle }

class BaseModel with ChangeNotifier {
  ViewState _state = ViewState.LogOut;
  ViewState _loading = ViewState.Idle;

  Map<String, dynamic> _userState = {'id': '', 'carInfor': '', 'turno': 0};

  get state => _state;
  get userState => _userState;
  get isLoading => _loading;

  set isLoading(ViewState value) {
    _loading = value;
    notifyListeners();
  }

  set userState(newValue) => _userState = newValue;

  set setState(ViewState value) {
    _state = value;
    notifyListeners();
  }
}
