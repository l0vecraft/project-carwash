import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/models/carwash.dart';
import 'package:carwash/core/models/reviews.dart';
import 'package:carwash/core/models/turn.dart';
import 'package:carwash/core/models/users.dart';
import 'package:carwash/core/services/api.dart';
import 'package:carwash/core/services/authServices.dart';
import 'package:carwash/core/services/carApi.dart';
import 'package:carwash/locator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserModel extends BaseModel {
  AuthServices _authServices = locator<AuthServices>();
  String _uid = '';
  String _idTurno = '';
  User _user = User();
  Map<String, dynamic> _data = {
    "uid": '',
    "name": '',
    "cid": '',
    "fecha": '',
    "turno": 0,
    "servicios": []
  };
  FirebaseAuth _auth = FirebaseAuth.instance;
  Api _api = locator<Api>();
  CarApi _carApi = locator<CarApi>();

  get uid => _uid;
  get data => _data;
  get idTurno => _idTurno;
  get user => _user;
  set data(newValue) => _data = newValue;
  set idTurno(newValue) => _idTurno = newValue;
  set uid(newValue) => _uid = newValue;

  Future<FirebaseUser> login(String email, String password) async {
    FirebaseUser result;
    isLoading = ViewState.Busy;
    try {
      result = await _authServices.login(email, password);
      if (result != null) {
        setState = ViewState.LogIn;
      }
      isLoading = ViewState.Idle;
      return result;
    } catch (e) {
      print(e);
    }
  }

  Future currentUser() async {
    isLoading = ViewState.Busy;
    FirebaseUser user = await _auth.currentUser();
    isLoading = ViewState.Idle;
    return user;
  }

  Future loginGoogle() async {
    isLoading = ViewState.Busy;
    FirebaseUser user = await _api.googleLogin();
    if (user != null) {
      setState = ViewState.LogIn;
      isLoading = ViewState.Idle;
      return user;
    }
  }

  Future loginFacebook() async {
    isLoading = ViewState.Busy;
    try {
      FirebaseUser user = await _api.facebookLogin();
      if (user != null) {
        setState = ViewState.LogIn;
        isLoading = ViewState.Idle;
        return user;
      }
    } catch (e) {
      print(e);
    }
    // print('USER === $user');
  }

  Future<String> currentUserId() async {
    isLoading = ViewState.Busy;
    _uid = await _api.currentUserId();
    isLoading = ViewState.Idle;
    return _uid;
  }

  bool logOut() {
    isLoading = ViewState.Busy;
    var result = _authServices.logOut();
    if (result) {
      setState = ViewState.LogOut;
    }
    isLoading = ViewState.Idle;
    return result;
  }

  Future addUser(User data) async {
    try {
      isLoading = ViewState.Busy;
      FirebaseUser user = (await _auth.createUserWithEmailAndPassword(
              email: data.email, password: data.password))
          .user;
      if (user != null) {
        setState = ViewState.LogIn;
        FirebaseUser result = await _api.addUser(data.toJson(), user.uid);
        isLoading = ViewState.Idle;
        return result;
      }
    } catch (e) {
      print(e);
    }
  }

  Future addGoogleUser() async {
    String errorMessage = '';
    isLoading = ViewState.Busy;
    FirebaseUser user = await _api.googleAddUser().catchError((onError) {
      setState = ViewState.LogOut;
      isLoading = ViewState.Idle;
      errorMessage = onError;
      print(errorMessage);
    });
    if (user != null) {
      setState = ViewState.LogIn;
      isLoading = ViewState.Idle;
      return user;
    } else {
      setState = ViewState.LogOut;
      isLoading = ViewState.Idle;
    }
  }

  Future addFacebookuser() async {
    isLoading = ViewState.Busy;
    FirebaseUser user = await _api.facebookAddUser();
    if (user != null) {
      setState = ViewState.LogIn;
      isLoading = ViewState.Idle;
    }
    return user;
  }

  Future<User> getUserById(String id) async {
    isLoading = ViewState.Busy;
    var doc = await _api.getUserById(id);
    if (doc != null) {
      _user = User.fromJson(doc.data);
      isLoading = ViewState.Idle;
      return _user;
    }
  }

  Future updateUser(User data, String id) async {
    isLoading = ViewState.Busy;
    try {
      FirebaseUser user = await _auth.currentUser();
      await _api.updateUser(data.toJson(), user.uid);
      await user.updateEmail(data.email);
      await user.updatePassword(data.password);
      // await getUserById(id);
    } catch (e) {
      print(e);
    }
    isLoading = ViewState.Idle;
  }

  Future removeUser(String id) async {
    isLoading = ViewState.Busy;
    FirebaseUser user = await _auth.currentUser();
    await _api.removeUser(user.uid);
    await user.delete();
    logOut();
    isLoading = ViewState.Idle;
  }

  Future<void> forgetPassword(String email) async {
    isLoading = ViewState.Busy;
    await _auth.sendPasswordResetEmail(email: email);
    isLoading = ViewState.Idle;
  }

  Future<String> getTurn(Map<String, dynamic> data, CarWash carWash) async {
    isLoading = ViewState.Busy;
    //! ya no existe espera y atendiendo
    int result = carWash.espera + carWash.atendiendo + 1;
    data["turno"] = result;
    Map carData = carWash.toJson();
    carData['capacity'] -= 1;
    try {
      var result = await _api.getTurn(data);
      await _carApi.updateCar(carData, carWash.id);
      return result;
    } catch (e) {
      print(e);
    }
    isLoading = ViewState.Idle;
  }

  Future getTurnState(String uid) async {
    // if (result != null) {
    //     data['uid'] = uid;
    //     data['cid'] = result['cid'];
    //     data['fecha'] = result['fecha'];
    //     data['turno'] = result['turno'];
    //     data['servicios'] = result['servicios'];
    //     data['idTurno'] = result['idTurno'];
    //   }
    Turn turn;
    List<DocumentSnapshot> result = [];
    try {
      result = await _api.getTurnState(uid);
      for (var i in result) {
        if (i.data['tipo'] != 'cancelado') turn = Turn.fromJson(i.data);
      }
      print('listo');
      return turn;
    } catch (e) {
      print(e);
    }
  }

  Future<void> cancelTurno(Map<String, dynamic> data, String id) async {
    isLoading = ViewState.Busy;
    data['turno'] = 0;
    try {
      await _api.cancelTurn(data, id);
      CarWash carData = await _carApi.getCarById(data['cid']);
      carData.capacity += 1;
      await _carApi.updateCar(carData.toJson(), carData.id);
      print('listo');
    } catch (e) {
      print(e);
    }
    isLoading = ViewState.Idle;
  }

  Future wirteReview(
      double rating, String text, String name, String cid) async {
    isLoading = ViewState.Busy;
    DateTime dateTime = DateTime.now();
    Reviews r = new Reviews(name, text, rating,
        Timestamp.fromMillisecondsSinceEpoch(dateTime.millisecondsSinceEpoch));
    listReviews.add(r);
    try {
      var result = await _api.writeReviews(r.toJson(), cid, _uid);
      if (result != null) {
        isLoading = ViewState.Idle;
      }
    } catch (e) {
      print(e);
    }
  }

  Future getHistory({String id = 'ysLPIDxVTjf5GsyluKVO'}) async {
    isLoading = ViewState.Busy;
    List<Turn> historial = [];
    try {
      var result = await _api.getHistory(id);
      if (historial.length != 0 || historial != null) {
        historial.addAll(result.documents.map((e) => Turn.fromJson(e.data)));
        isLoading = ViewState.Idle;
        return historial;
      }
    } catch (e) {
      print(e);
    }
  }
}
