import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/models/carwash.dart';
import 'package:carwash/core/services/carApi.dart';
import 'package:carwash/locator.dart';

class CarModel extends BaseModel {
  int _total = 0;
  String _cid = '';
  CarApi _carApi = locator<CarApi>();
  CarWash _lavadero;

  get lavadero => _lavadero;
  get cid => _cid;
  get total => _total;
  set total(newValue) => _total = newValue;
  set cid(newValue) => _cid = newValue;

  Future getAllCarWash() async {
    List<CarWash> lavaderos = [];
    isLoading = ViewState.Busy;
    var result = await _carApi.getAllCars();
    if (result != null) {
      lavaderos.addAll(result.documentChanges.map(
          (e) => CarWash.fromJson(e.document.data, e.document.documentID)));
      isLoading = ViewState.Idle;

      return lavaderos;
    }
  }

  Future getCarById(String uid) async {
    isLoading = ViewState.Busy;
    try {
      CarWash lavadero = await _carApi.getCarById(uid);
      if (lavadero != null) {
        _lavadero = lavadero;
        isLoading = ViewState.Idle;
      }
      return lavadero;
    } catch (e) {
      print(e);
    }
  }

  Future updateCar(String cid, Map data) async {
    isLoading = ViewState.Busy;
    try {
      await _carApi.updateCar(data, cid);
      isLoading = ViewState.Idle;
    } catch (e) {
      print(e);
    }
  }

  // int length() {
  //   return _lavaderos.length;
  // }

  // void getCarWash(int i) {
  //   _lavadero = _api.getCarWash(i);
  //   notifyListeners();
  // }
}
/**
     * ?para tomar el turno como es necesito
     * * 1. el ID del usuario
     * * 2. el ID del lavadero (aunque aqui sera el nombre para ahorrar)
     * * 3. su turno
     * * 4. la fecha y si la fecha es la misma que hoy entonces darle un turno
     *  *   el problema estaria en, como configurarlo con la hora (((pensando)))
     * ?pensaria que todo esto puede ir en una tabla, pero como no esta
     * ? hay que crearla aqui a lo mal
     */
