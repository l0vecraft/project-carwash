import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/models/carwash.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CarApi extends BaseModel {
  final Firestore _db = Firestore.instance;

  Future<QuerySnapshot> getAllCars() async {
    // return await _db.collection('washes').getDocuments();
    var result = await _db.collection('washes').getDocuments();
    return result;
  }

  Future<CarWash> getCarById(String id) async {
    CarWash car;
    var result = await getAllCars();
    result.documents.map((e) {
      if (e.documentID == id) {
        car = CarWash.fromJson(e.data, id);
      }
    }).toList()[0];
    return car;
  }

  Future updateCar(Map data, String id) async {
    var carToUpdate =
        await _db.collectionGroup('myWashes').where('id', isEqualTo: id);
    // ({
    //   "atendiendo": data['atendiendo'],
    //   "dir": data['dir'],
    //   "dis": data['dis'],
    //   "empleados": data['empleados'],
    //   "espera": data['espera'],
    //   "location": data['espera'],
    //   "name": data['name'],
    //   "reviews": data['reviews'],
    //   "score": data['score'],
    //   "servicios": data['servicios']
    // });
  }
}
