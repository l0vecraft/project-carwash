import 'package:carwash/core/models/turn.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

class Api {
  final Firestore _db =
      Firestore.instance; //el objeto para manipular los metodos
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  Future<FirebaseUser> login(String email, String password) async {
    try {
      FirebaseUser user = (await _auth.signInWithEmailAndPassword(
              email: email, password: password))
          .user;
      if (user != null) {
        return user;
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> logOut() async {
    await _auth.signOut();
  }

  Future<FirebaseUser> googleLogin() async {
    try {
      final GoogleSignInAccount googleUser =
          await _googleSignIn.signIn().catchError((onError) {
        if (onError == GoogleSignIn.kSignInCanceledError ||
            onError == GoogleSignIn.kSignInCanceledError) return null;
      });
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication.catchError((onError) {
        if (onError == GoogleSignIn.kSignInCanceledError ||
            onError == GoogleSignIn.kSignInCanceledError) return null;
      });
      final AuthCredential credentials = GoogleAuthProvider.getCredential(
          accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
      FirebaseUser user = (await _auth.signInWithCredential(credentials)).user;
      if (user != null) {
        return user;
      }
    } catch (e) {
      print(e);
    }
  }

  Future facebookLogin() async {
    final facebookLogin = FacebookLogin();
    final FacebookLoginResult result = await facebookLogin.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        try {
          final FacebookLoginResult result =
              await facebookLogin.logIn(['email']);
          final AuthCredential authCredential =
              FacebookAuthProvider.getCredential(
            accessToken: result.accessToken.token,
          );
          final FirebaseUser user =
              (await _auth.signInWithCredential(authCredential)).user;
          print("API: ${user.displayName}");
          return user;
        } catch (e) {
          print(e);
        }
        break;

      case FacebookLoginStatus.cancelledByUser:
        print('cacelado por usuario');
        return null;
      case FacebookLoginStatus.error:
        print('ERROR: ${result.errorMessage}');
        return null;
      default:
        return ('por defecto');
    }
  }

  Future<FirebaseUser> facebookAddUser() async {
    final user = await facebookLogin();
    if (user != null) {
      var data = {"username": user.displayName, "email": user.email};
      addUserWithCredentials(data, user.uid);
      print(data);
    }
    return user;
  }

  Future<FirebaseUser> googleAddUser() async {
    String errorMessage = '';
    try {
      final GoogleSignInAccount googleUser =
          await _googleSignIn.signIn().catchError((onError) {
        if (onError == GoogleSignIn.kSignInCanceledError) {
          errorMessage = GoogleSignIn.kSignInCanceledError;
          return errorMessage;
        } else {
          errorMessage = GoogleSignIn.kSignInFailedError;
          return errorMessage;
        }
      });
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication.catchError((onError) {
        if (onError == GoogleSignIn.kSignInCanceledError) {
          errorMessage = GoogleSignIn.kSignInCanceledError;
          return errorMessage;
        } else {
          errorMessage = GoogleSignIn.kSignInFailedError;
          return errorMessage;
        }
      });
      final AuthCredential credentials = GoogleAuthProvider.getCredential(
          accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
      FirebaseUser user = (await _auth.signInWithCredential(credentials)).user;
      if (user != null) {
        var data = {"username": user.displayName, "email": user.email};
        addUserWithCredentials(data, user.uid);
        return user;
      }
    } catch (e) {
      print(e);
    }
  }

  Future<String> currentUserId() async {
    FirebaseUser user = await _auth.currentUser();
    return user.uid;
  }

  Future addUser(Map data, String id) {
    //hago un insert en la base de datos
    //? se pudo haber usado ref.add(data), pero de esa forma
    //? no ibamos a poder modificar y asignar el mismo id, por lo menos no sabria
    return _db.collection('users').document(id).setData({
      'username': data['username'],
      'email': data['email'],
      'password': data['password'],
      'turno': 0
    });
  }

  Future addUserWithCredentials(Map data, String id) {
    return _db.collection('users').document(id).setData(
        {'username': data['username'], 'email': data['email'], 'turno': 0});
  }

  Future getUserById(String uid) {
    return _db.collection('users').document(uid).get();
  }

  Future<void> updateUser(Map data, String id) {
    return _db.collection('users').document(id).setData({
      'username': data['username'],
      'email': data['email'],
      'password': data['password'],
      'turno': 0
    });
  }

/*
---------------------------------------------------------------------------
  a estos metodos de abajo les hacen faltan cosas por terminar para estar 
  completos como tal
  TODO: revisar la fecha y si es hoy asignarle automaticamente un turno
        * el turno se asignaria regresando una lista de turnos con el tipo "reservado"
        * si es 0 entonces asignarle listaTurnos.lenght+1 como turno ((siempre!))
      !si la fecha no era hoy entonces hacer el mismo procedimiento  
  TODO: plantear alguna comparacion por hora ((a futuro))
*/
  Future getTurn(Map data) async {
    String uid = data['uid'];
    await _db
        .collection('users')
        .document(uid)
        .updateData({"turno": data["turno"]});

    return await _db
        .collection('turns')
        .document(data['cid'])
        .collection('myTurn')
        .add({
      "uid": data['uid'],
      "cid": data['cid'],
      "idTurno": '',
      "name": data['name'],
      "total": data['total'],
      "servicios": data['services'],
      "fecha": data['fecha'],
      "turno": data['turno'],
      "tipo": "reservado"
    }).then((DocumentReference value) {
      _db
          .collection('turns')
          .document(data['cid'])
          .collection('myTurn')
          .document(value.documentID)
          .updateData({
        "idTurno": value.documentID,
      });
      return value.documentID;
    });
  }

  Future getTurnState(String uid) async {
    List<Turn> turnos = [];
    var result = await _db
        .collectionGroup('myTurn')
        .where('uid', isEqualTo: uid)
        //? esta de abajo hay que ver que se hace en la vista
        //? para cuando cambia a finalizado salte otra pantalla
        // .where('tipo', isEqualTo: 'reservado')
        .getDocuments();

    // for (var i in result.documents) {
    //   turnos.add(Turn.fromJson(i.data));
    //   print(i.data);
    // }
    return result.documents;
  }

  Future<void> cancelTurn(Map data, String id) async {
    await _db
        .collection('users')
        .document(data['uid'])
        .updateData({"turno": data["turno"]});
    return _db
        .collection('turns')
        .document(data['cid'])
        .collection('myTurn')
        .document(id)
        .updateData({"tipo": "cancelado"});
  }

  Future<void> removeUser(String id) {
    return _db.collection('users').document(id).delete();
  }

  Future writeReviews(Map data, String cid, String uid) {
    return _db
        .collection('reviews')
        .document(cid)
        .collection('comments')
        .document(uid)
        .setData(data);
  }

  Future<QuerySnapshot> getHistory(String id) {
    return _db
        .collection('history')
        .document(id)
        .collection('myAccount')
        .getDocuments();
  }
}
