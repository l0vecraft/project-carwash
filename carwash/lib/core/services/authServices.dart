import 'dart:async';

import 'package:carwash/core/controller/baseModel.dart';
import 'package:carwash/core/services/api.dart';
import 'package:carwash/locator.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthServices extends BaseModel {
  bool _logged = false;
  Api _api = locator<Api>();
  StreamController<bool> authController = StreamController<bool>();

  Future<FirebaseUser> login(String email, String password) async {
    FirebaseUser user = await _api.login(email, password);
    if (user != null) {
      //cambio el estado
      _logged = true;
      authController.add(_logged);
    }
    return user;
  }

  Future<FirebaseUser> facebookLogin() async {
    FirebaseUser user = await _api.facebookLogin();
    if (user != null) {
      _logged = true;
      authController.add(_logged);
    }
    return user;
  }

  bool logOut() {
    try {
      _api.logOut();
      _logged = false;
      authController.add(_logged);
    } catch (e) {
      print(e);
    }
    return _logged;
  }
}
