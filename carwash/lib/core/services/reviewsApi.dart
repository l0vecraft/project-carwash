import 'package:cloud_firestore/cloud_firestore.dart';

class ReviewsApi {
  final Firestore _db = Firestore.instance;

  Future getAllComments() {
    return _db.collectionGroup('comments').getDocuments();
  }
}
