import 'package:cloud_firestore/cloud_firestore.dart';

class Reviews {
  String name;
  String description;
  num score;
  Timestamp fecha;

  Reviews(this.name, this.description, this.score, this.fecha);

  Reviews.fromJson(Map json) {
    name = json['name'];
    description = json['description'];
    score = json['score'];
    fecha = json['fecha'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = this.name;
    data['description'] = this.description;
    data['score'] = this.score;
    data['fecha'] = this.fecha;
    return data;
  }
}

List<Reviews> listReviews = [
  Reviews(
      'juan',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lacinia, risus in egestas fermentum, metus orci dapibus turpis, in mollis nisi elit a tortor',
      3.5,
      Timestamp.fromMillisecondsSinceEpoch(
          DateTime.now().millisecondsSinceEpoch)),
  Reviews(
      'esteban',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lacinia, risus in egestas fermentum, metus orci dapibus turpis, in mollis nisi elit a tortor',
      2.0,
      Timestamp.fromMillisecondsSinceEpoch(
          DateTime.now().millisecondsSinceEpoch)),
  Reviews(
      'erick',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lacinia, risus in egestas fermentum, metus orci dapibus turpis, in mollis nisi elit a tortor',
      5.0,
      Timestamp.fromMillisecondsSinceEpoch(
          DateTime.now().millisecondsSinceEpoch)),
  Reviews(
      'sebas',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lacinia, risus in egestas fermentum, metus orci dapibus turpis, in mollis nisi elit a tortor',
      1.5,
      Timestamp.fromMillisecondsSinceEpoch(
          DateTime.now().millisecondsSinceEpoch)),
  Reviews(
      'edil',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lacinia, risus in egestas fermentum, metus orci dapibus turpis, in mollis nisi elit a tortor',
      3.0,
      Timestamp.fromMillisecondsSinceEpoch(
          DateTime.now().millisecondsSinceEpoch)),
  Reviews(
      'carlos',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lacinia, risus in egestas fermentum, metus orci dapibus turpis, in mollis nisi elit a tortor',
      0,
      Timestamp.fromMillisecondsSinceEpoch(
          DateTime.now().millisecondsSinceEpoch)),
  Reviews(
      'john',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lacinia, risus in egestas fermentum, metus orci dapibus turpis, in mollis nisi elit a tortor',
      4.5,
      Timestamp.fromMillisecondsSinceEpoch(
          DateTime.now().millisecondsSinceEpoch)),
  Reviews(
      'bradd',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lacinia, risus in egestas fermentum, metus orci dapibus turpis, in mollis nisi elit a tortor',
      2.5,
      Timestamp.fromMillisecondsSinceEpoch(
          DateTime.now().millisecondsSinceEpoch)),
];
