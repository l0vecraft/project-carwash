class User {
  String email;
  String password;
  int turno;
  String username;

  User({this.email, this.password, this.turno, this.username});

  User.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    turno = json['turno'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['email'] = this.email;
    data['password'] = this.password;
    data['turno'] = this.turno;
    data['username'] = this.username;
    return data;
  }
}
