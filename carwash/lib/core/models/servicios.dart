class Servicios {
  String name;
  String id;
  String description;
  int price;
  int duration;

  Servicios(
    this.name,
    this.id,
    this.description,
    this.price,
    this.duration,
  );

  Servicios.fromJson(Map<String, dynamic> json, String sid) {
    name = json['name'];
    id = sid;
    description = json['description'];
    price = json['price'];
    duration = json['duration'];
  }

  Servicios.fromMap(Map snapshot, String sid) {
    name = snapshot['name'];
    id = sid;
    description = snapshot['description'];
    price = snapshot['price'];
    duration = snapshot['duration'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['price'] = this.price;
    data['duration'] = this.duration;
    return data;
  }
}
