import 'package:cloud_firestore/cloud_firestore.dart';

class Turn {
  String cid;
  Timestamp fecha;
  String idTurno;
  List<dynamic> servicios;
  String tipo;
  int turno;
  String uid;
  String name;
  int total;

  Turn(
      {this.cid,
      this.name,
      this.fecha,
      this.idTurno,
      this.servicios,
      this.tipo,
      this.turno,
      this.uid,
      this.total});

  Turn.fromJson(Map<String, dynamic> json) {
    cid = json['cid'] ?? '';
    name = json['name'] ?? '';
    fecha = json['fecha'];
    idTurno = json['idTurno'] ?? '';
    servicios = json['servicios'] ?? [];
    tipo = json['tipo'] ?? '';
    turno = json['turno'] ?? 0;
    uid = json['uid'] ?? '';
    total = json['total'] ?? 0;
  }

  Turn.fromMap(Map snapshot) {
    cid = snapshot['cid'] ?? '';
    name = snapshot['name'] ?? '';
    fecha = snapshot['fecha'];
    idTurno = snapshot['idTurno'] ?? '';
    servicios = snapshot['servicios'] ?? [];
    tipo = snapshot['tipo'] ?? '';
    turno = snapshot['turno'] ?? 0;
    uid = snapshot['uid'] ?? '';
    total = snapshot['total'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cid'] = this.cid ?? '';
    data['name'] = this.name ?? '';
    data['fecha'] = this.fecha ?? '';
    data['idTurno'] = this.idTurno ?? '';
    data['servicios'] = this.servicios ?? [];
    data['tipo'] = this.tipo ?? '';
    data['turno'] = this.turno ?? 0;
    data['uid'] = this.uid ?? '';
    data['total'] = this.total ?? '';
    return data;
  }
}
