import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CarWash {
  String id;
  int atendiendo;
  String dir;
  num dis;
  int empleados;
  int espera;
  GeoPoint location;
  String name;
  List reviews;
  num score;
  List servicios;
  int capacity;

  CarWash(
      {this.atendiendo,
      this.id,
      this.dir,
      this.dis,
      this.empleados,
      this.espera,
      this.location,
      this.name,
      this.reviews,
      this.score,
      this.capacity,
      this.servicios});

  CarWash.fromJson(Map<String, dynamic> json, String uid) {
    id = uid;
    atendiendo = json['atendiendo'] ?? 0;
    dir = json['dir'];
    dis = json['dis'] ?? 0.0;
    empleados = json['empleados'];
    espera = json['espera'] ?? 0;
    location = json['location'];
    name = json['name'];
    reviews = json['reviews'];
    score = json['score'] ?? 0.0;
    capacity = json['capacity'] ?? 0;
    servicios = json['servicios'];
  }
  CarWash.fromMap(Map snapshot, String uid)
      : id = uid ?? '',
        atendiendo = snapshot['atendiendo'] ?? 0,
        dir = snapshot['dir'] ?? '',
        dis = snapshot['dis'] ?? 0.0,
        empleados = snapshot['empleados'] ?? 0,
        espera = snapshot['espera'] ?? 0,
        location = snapshot['location'] ?? LatLng(10.3997200, -75.5144400),
        name = snapshot['name'] ?? '',
        reviews = snapshot['reviews'] ?? [],
        score = snapshot['score'] ?? 0.0,
        capacity = snapshot['capacity'] ?? 0,
        servicios = snapshot['servicios'] ?? [];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['atendiendo'] = this.atendiendo ?? 0;
    data['dir'] = this.dir;
    data['dis'] = this.dis;
    data['empleados'] = this.empleados;
    data['espera'] = this.espera ?? 0;
    data['location'] = this.location;
    data['name'] = this.name;
    data['reviews'] = this.reviews;
    data['score'] = this.score;
    data['capacity'] = this.capacity;
    data['servicios'] = this.servicios;
    return data;
  }
}

// List<CarWash> lavaderos = [
//   CarWash(
//       name: 'lavadero 1',
//       dir: 'Direccion 1 larga',
//       dis: 0.7,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             30,
//             false),
//         Servicios(
//             'Restauracion de Luces',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             28,
//             40,
//             false),
//         Servicios(
//             'Lavado de tapiceria',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             45,
//             27,
//             false),
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             30,
//             false),
//         Servicios(
//             'Restauracion de Luces',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             28,
//             40,
//             false),
//         Servicios(
//             'Lavado de tapiceria',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             45,
//             27,
//             false),
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             30,
//             false),
//         Servicios(
//             'Restauracion de Luces',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             28,
//             40,
//             false),
//         Servicios(
//             'Lavado de tapiceria',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             45,
//             27,
//             false),
//       ],
//       score: 3.2,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(13, 23.65)),
//   CarWash(
//       name: 'lavadero 2',
//       dir: 'Direccion 1 larga',
//       dis: 1.2,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             30,
//             false)
//       ],
//       score: 2.5,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(10.399901, -75.481305)),
//   CarWash(
//       name: 'lavadero 3',
//       dir: 'Direccion 1 larga',
//       dis: 1.0,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             40,
//             false)
//       ],
//       score: 4.0,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(10.4005037, -75.4901314)),
//   CarWash(
//       name: 'lavadero 4',
//       dir: 'Direccion 1 larga',
//       dis: 3.0,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             20,
//             false)
//       ],
//       score: 3.0,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(10.3974132, -75.5114928)),
//   CarWash(
//       name: 'lavadero 5',
//       dir: 'Direccion 1 larga',
//       dis: 2.8,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             30,
//             false)
//       ],
//       score: 4.8,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(10.383148, -75.501033)),
//   CarWash(
//       name: 'lavadero 6',
//       dir: 'Direccion 1 larga',
//       dis: 0.1,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             29,
//             false)
//       ],
//       score: 5.0,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(10.413726, -75.532934)),
//   CarWash(
//       name: 'lavadero 7',
//       dir: 'Direccion 1 larga',
//       dis: 1.7,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             30,
//             false)
//       ],
//       score: 0.0,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(10.3934369, -75.5097269)),
//   CarWash(
//       name: 'lavadero 8',
//       dir: 'Direccion 1 larga',
//       dis: 1.9,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             25,
//             false)
//       ],
//       score: 4.2,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(10.391229, -75.522232)),
//   CarWash(
//       name: 'lavadero 9',
//       dir: 'Direccion 1 larga',
//       dis: 0.5,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             40,
//             false)
//       ],
//       score: 3.3,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(10.436764, -75.527773)),
//   CarWash(
//       name: 'lavadero 10',
//       dir: 'Direccion 1 larga',
//       dis: 4.7,
//       empleados: 10,
//       servicios: [
//         Servicios(
//             'Lavado y aspirado',
//             'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//             12,
//             20,
//             false)
//       ],
//       score: 2.9,
//       reviews: [
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin fringilla cursus finibus. Vivamus ligula enim, molestie vel hendrerit non, aliquet ac lectus. Donec condimentum enim eu eros rhoncus molestie.',
//         'Nunc blandit lacinia finibus. Praesent venenatis non augue sed laoreet. Ut suscipit sapien id pulvinar pellentesque. Aenean aliquet ex nec augue lobortis vestibulum',
//         'Integer eget risus fermentum, molestie diam a, placerat ipsum. Vivamus malesuada neque at libero feugiat maximus. Vivamus nulla tellus, efficitur a nisi nec, interdum fermentum nisi. Donec dapibus finibus nulla sed pellentesque',
//         'Proin non ligula nec lacus commodo hendrerit ac et nisl. Mauris sit amet enim nisi. Pellentesque elementum euismod nulla, eget rhoncus ex condimentum vitae. Suspendisse potenti. Aenean luctus elementum velit, quis accumsan nulla'
//       ],
//       espera: 5,
//       atendiendo: 4,
//       address: LatLng(10.407696, -75.527557)),
// ];
