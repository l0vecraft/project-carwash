class Historial {
  String title;
  String subtitle;
  String date;
  int price;

  Historial(this.title, this.subtitle, this.price, this.date);
}

List<Historial> listaH = [
  Historial('Lavadero 1', 'Lavado y aspirado', 12, 'enero 12'),
  Historial('Lavadero 1', 'Lavado y aspirado', 12, 'enero 19'),
  Historial('Lavadero 1', 'Restauracion de Luces', 28, 'marzo 29'),
  Historial('Lavadero 1', 'Lavado de tapiceria', 45, 'abril 29'),
];
